package com.pion.adviceapp;


import android.app.FragmentManager;

import android.support.annotation.NonNull;
import android.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import android.support.design.widget.BottomNavigationView;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.pion.adviceapp.fragments.BasicActivityFragment;
import com.pion.adviceapp.fragments.FavoriteFragment;


public class MainActivity extends AppCompatActivity {

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            FragmentManager fragmentManager = getFragmentManager();
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    try {
                        Fragment fragment = BasicActivityFragment.class.newInstance();
                        fragmentManager.beginTransaction().replace(R.id.fragmentFrame, fragment).commit();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    return true;
                case R.id.navigation_dashboard:
                    try {
                        Fragment fragment = FavoriteFragment.class.newInstance();
                        fragmentManager.beginTransaction().replace(R.id.fragmentFrame, fragment).commit();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        try {
            Fragment fragment = BasicActivityFragment.class.newInstance();
            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.fragmentFrame, fragment).commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //getMenuInflater().inflate(R.menu.menu_basic, menu);
        return true;
    }
}
