package com.pion.adviceapp.models;

import com.google.gson.annotations.SerializedName;

public class Advice {
    //@SerializedName("id")
    Integer id;
    //@SerializedName("text")
    String text;
    //@SerializedName("sound")
    String sound;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getSound() {
        return sound;
    }

    public void setSound(String sound) {
        this.sound = sound;
    }


}
