package com.pion.adviceapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBManager extends SQLiteOpenHelper {

    final private static String dbName = "advice.db";
    final private static int dbVersion = 1;

    public static final String ADVICE_TABLE_NAME = "advice";
    public static final String ADVICE_COLUMN_ID = "_id";
    public static final String ADVICE_COLUMN_NUMBER = "num";
    public static final String ADVICE_COLUMN_TEXT = "text";
    public static final String ADVICE_COLUMN_SOUND = "sound";


    public DBManager(Context context) {
        super(context, dbName, null, dbVersion);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS " + ADVICE_TABLE_NAME + " ("+ADVICE_COLUMN_ID+" INTEGER PRIMARY KEY AUTOINCREMENT, "+ADVICE_COLUMN_NUMBER+" INTEGER, "+ ADVICE_COLUMN_TEXT +" TEXT, "+ ADVICE_COLUMN_SOUND + " TEXT)");


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (newVersion >= oldVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + ADVICE_TABLE_NAME);
            onCreate(db);
        }
    }

    public void insertAdvice(Integer number, String text, String sound) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(ADVICE_COLUMN_NUMBER, number);
        contentValues.put(ADVICE_COLUMN_TEXT, text);
        contentValues.put(ADVICE_COLUMN_SOUND, sound);
        db.insert(ADVICE_TABLE_NAME, null, contentValues);
    }

    public Cursor getAllAdvices() {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.rawQuery("SELECT * FROM " + ADVICE_TABLE_NAME + " ORDER BY " +ADVICE_COLUMN_ID, null);
    }

    public void clearAdviceTable() {
        SQLiteDatabase db = this.getReadableDatabase();
        db.execSQL("DROP TABLE IF EXISTS " + ADVICE_TABLE_NAME);
        onCreate(db);
    }

    public Integer deleteAdvice(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(ADVICE_TABLE_NAME, ADVICE_COLUMN_ID + " = ?", new String[]{Integer.toString(id)});
    }



}
