package com.pion.adviceapp;

import com.pion.adviceapp.models.Advice;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;

public class AdviceAPI {
    private static final String BASE_URL = "http://fucking-great-advice.ru/api/";

    private static Retrofit retrofit = null;

    public interface ApiInterface {
        @GET("random")
        Call<Advice> getAdvice();
    }
    public static Retrofit getClient() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        }
        return retrofit;
    }
}
