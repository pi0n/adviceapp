package com.pion.adviceapp;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pion.adviceapp.models.Advice;

import java.util.List;

public class AdviceListAdapter extends RecyclerView.Adapter<AdviceListAdapter.MyViewHolder> {
    private Context context;
    private List<Advice> adviceList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView description;
        public RelativeLayout viewBackground, viewForeground;

        public MyViewHolder(View view) {
            super(view);
            description = view.findViewById(R.id.description);
            viewBackground = view.findViewById(R.id.view_background);
            viewForeground = view.findViewById(R.id.view_foreground);
        }
    }


    public AdviceListAdapter(Context context, List<Advice> cartList) {
        this.context = context;
        this.adviceList = cartList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.advice_view, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final Advice item = adviceList.get(position);
        holder.description.setText(item.getText());
    }

    @Override
    public int getItemCount() {
        return adviceList.size();
    }

    public void removeItem(int position) {
        adviceList.remove(position);
        // notify the item removed by position
        // to perform recycler view delete animations
        // NOTE: don't call notifyDataSetChanged()
        notifyItemRemoved(position);
    }

    public void restoreItem(Advice item, int position) {
        adviceList.add(position, item);
        notifyItemInserted(position);
    }
}
