package com.pion.adviceapp.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.pion.adviceapp.DBManager;
import com.pion.adviceapp.MainActivity;
import com.pion.adviceapp.R;

import com.pion.adviceapp.AdviceAPI;
import com.pion.adviceapp.models.Advice;

import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.support.constraint.Constraints.TAG;

public class BasicActivityFragment extends Fragment {

    TextView tvAdvice;
    DBManager dbManager;

    Button refresh;
    Button favorites;

    Advice data;
    AdviceAPI.ApiInterface api;

    ConstraintLayout constraintLayout;
    Timer timer;

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (container != null) {
            container.removeAllViews();
        }
        View view = inflater.inflate(R.layout.fragment_basic, container, false);
        initViews(view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater){
        super.onCreateOptionsMenu(menu, inflater);
        //menu.clear();
        inflater.inflate(R.menu.menu_basic, menu);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_bookmarks:
                addToFavorites();
                return true;
            case R.id.action_refresh:
                timer.cancel();
                cycle();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void initViews(View v) {
        constraintLayout = v.findViewById(R.id.basic_layout);
        tvAdvice = v.findViewById(R.id.message);
        refresh = v.findViewById(R.id.btn_refresh);
        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getAdvice();
            }
        });
        favorites = v.findViewById(R.id.btn_favorites);
        favorites.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addToFavorites();
            }
        });
        dbManager = new DBManager(getActivity());
        api = AdviceAPI.getClient().create(AdviceAPI.ApiInterface.class);
        cycle();
    }
    private void addToFavorites() {
        //Toast.makeText(getActivity(), "Совет добавлен в избранное", Toast.LENGTH_LONG).show();
        if (data == null) {
            Toast.makeText(getActivity(), "Добавлять нечего", Toast.LENGTH_LONG).show();
            return;
        }
        /*Snackbar snackbar = Snackbar
                .make(constraintLayout, "Добавлено в избранное", Snackbar.LENGTH_LONG);
        snackbar.show();*/
        Toast.makeText(getActivity(), "Совет добавлен в избранное", Toast.LENGTH_LONG).show();
        dbManager.insertAdvice(data.getId(), data.getText(), data.getSound());
        getAdvice();
    }

    private void cycle() {
        timer=new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                getAdvice();
            }
        }, 0, 30000);
       /* new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                getAdvice();
            }
        }, 0L, 30000L);*/
    }

    public void getAdvice() {

        Call<Advice> callAdvice = api.getAdvice();
        callAdvice.enqueue(new Callback<Advice>() {
            @Override
            public void onResponse(Call<Advice> call, Response<Advice> response) {
                Log.d(TAG, "Advice GET OK");
                data = response.body();
                if (response.isSuccessful()) {
                    tvAdvice.setText(data.getText());
                }
            }

            @Override
            public void onFailure(Call<Advice> call, Throwable t) {
                Toast.makeText(getActivity(), "Произошла ошибка", Toast.LENGTH_SHORT).show();
 /*               Snackbar snackbar = Snackbar
                        .make(constraintLayout, "Произошла ошибка", Snackbar.LENGTH_LONG);
                snackbar.show();*/
                Log.d(TAG, "Advice GET Error", t);
            }
        });
    }

    @Override
    public void onStop() {
        timer.cancel();
        super.onStop();
    }
}
