package com.pion.adviceapp.fragments;

import android.app.Fragment;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pion.adviceapp.AdviceListAdapter;
import com.pion.adviceapp.DBManager;
import com.pion.adviceapp.R;
import com.pion.adviceapp.RecyclerItemTouchHelper;
import com.pion.adviceapp.models.Advice;

import java.util.ArrayList;
import java.util.List;

/**
 * A placeholder fragment containing a simple view.
 */
public class FavoriteFragment extends Fragment implements RecyclerItemTouchHelper.RecyclerItemTouchHelperListener {
    List<Advice> adviceList;

    RecyclerView recyclerView;
    ConstraintLayout constraintLayout;

    DBManager dbManager;

    AdviceListAdapter adviceListAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dbManager = new DBManager(getActivity());
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_fav, container, false);
        initViews(view);
        return view;
    }

    public void initViews(View v) {
        recyclerView = v.findViewById(R.id.recycler_view);
        constraintLayout = v.findViewById(R.id.constraintlayout);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater){
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        //inflater.inflate(R.menu.your_menu, menu);
    }

    @Override
    public void onResume() {
        super.onResume();

        Cursor cursor = dbManager.getAllAdvices();
        if (cursor.getCount() > 0) {
            adviceList = new ArrayList<>();
            for(cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                Advice advice = new Advice();
                advice.setId(cursor.getInt(1));
                advice.setText(cursor.getString(2));
                advice.setSound(cursor.getString(3));
                adviceList.add(advice);
            }
            adviceListAdapter = new AdviceListAdapter(getActivity().getApplicationContext(), adviceList);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
            recyclerView.setAdapter(adviceListAdapter);

            ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new RecyclerItemTouchHelper(0, ItemTouchHelper.LEFT, this);
            new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(recyclerView);

        }
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {
        if (viewHolder instanceof AdviceListAdapter.MyViewHolder) {
            final Advice deletedItem = adviceList.get(viewHolder.getAdapterPosition());
            final int deletedIndex = viewHolder.getAdapterPosition();

            adviceListAdapter.removeItem(viewHolder.getAdapterPosition());

            Snackbar snackbar = Snackbar
                    .make(constraintLayout, "Совет удален из избранного", Snackbar.LENGTH_LONG);
            snackbar.setAction("Отменить", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    adviceListAdapter.restoreItem(deletedItem, deletedIndex);
                }
            });
            snackbar.setActionTextColor(Color.YELLOW);
            snackbar.show();
        }
    }
}
